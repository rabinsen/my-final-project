<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $fillable = [
//        'title',
//        'price',
//        'status',
//        'readyToMove',
//        'address',
//        'city',
//        'country',
//        'mapLocation',
//        'landArea',
//        'houseArea',
//        'plotted',
//        'storey',
//        'bedroom',
//        'bathroom',
//        'kitchen',
//        'roadDistance',
//        'description',
        'image'


    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function categories(){
        return $this->belongsToMany('App\Category');
    }
}
