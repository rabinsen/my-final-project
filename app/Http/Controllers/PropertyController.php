<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;

use Carbon\Carbon;
use App\Http\Requests;
use Auth;
use Image;
use Illuminate\Support\Facades\Input;




class PropertyController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('auth',['only' => 'create']);
//    }

    public function upload(){
        return view('create');
    }

//    public function show(){
//        $properties = Property::latest()->get();
//        return view('properties.show', compact('properties'));
//    }

    public function store(Request $request)
    {
        $property = new Property();



//        $this->validate($request, [
//            //'category' => 'required',
//            'title' => 'required',
//            'price' => 'required',
//            //'type' => 'required',
////            'status' => 'required',
////            'readyToMove' => 'required',
////            'address' => 'required',
////            'city' => 'required',
////            'country' => 'required',
////            'mapLocation' => 'required',
////            'landArea' => 'required',
////            'houseArea' => 'required',
////            'plotted' => '',
////            'storey' => 'required',
////            'bedroom' => 'required',
////            'bathroom' => 'required',
////
////            'kitchen' => 'required',
////            'roadDistance' => '',
////            'description' => '',
//            'image' => 'required'
//        ]);
        $property->title = $request->title;
//        $property->price = $request->price;
//        //$property->type = $request->type;
//        $property->status = $request->status;
//        $property->readyToMove = $request->readyToMove;
//        $property->address = $request->address;
//        $property->city = $request->city;
//        $property->country = $request->country;
//        $property->mapLocation = $request->mapLocation;
//        $property->landArea = $request->landArea;
//        $property->houseArea = $request->houseArea;
//        $property->plotted = $request->plotted;
//        $property->storey = $request->storey;
//        $property->bedroom = $request->bedroom;
//        $property->bathroom = $request->bathroom;
//        $property->kitchen = $request->kitchen;
//        $property->roadDistance = $request->roadDistance;
//        $property->description = $request->description;
//


        //return redirect()->route('showProperties')->with(['success' => 'Image Uploaded Successfully']);

        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(300, 300)->save( public_path('/images/' . $filename ) );

            //$user = Auth::user();
            $property->image = $filename;
            // $user->save();
            Auth::user()->properties()->save($property);
        }



    }




}
