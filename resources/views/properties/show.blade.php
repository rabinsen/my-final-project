@extends('layouts.app)

@section('content')

    <h1>Articles</h1>
    <hr>
    @foreach($properties as $property)

        <li>
            <a href="{{ url('/images/'.$image->filePath) }}" target="_blank">
                <img src="{{ url('/images/'.$image->filePath) }}">
            </a>
        </li>

    @endforeach

@endsection