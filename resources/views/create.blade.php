@extends('layouts.app')

@section('content')
    <div class="form-group col-md-4" >
    <form enctype="multipart/form-data" action="store" method="POST">

        <div class="form-group">
            <label>Title:</label>
            <input type="text" name="title">

        </div>

        <div class="form-group">
            <label>Upload Image</label>
            <input type="file" name="image">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

        </div>

        <div>
            <input type="submit" class="pull-right btn btn-sm btn-primary">
        </div>

    </form>
        </div>

    {{--<div class="form-group col-md-4" >--}}
        {{--{!! Form::open(['action' => 'PropertyController@store']) !!}--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::label('category', 'Category:') !!}--}}
            {{--{!! Form::text('category', null, ['class' => 'form-control']) !!}--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::label('title', 'Title:') !!}--}}
            {{--{!! Form::text('title', null, ['class' => 'form-control']) !!}--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::label('price', 'Price:') !!}--}}
            {{--{!! Form::text('price', null, ['class' => 'form-control']) !!}--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::label('type', 'Type:') !!}--}}
            {{--{!! Form::radio('type', 'Residential', ['class' => 'form-control']) !!} Residential--}}
            {{--{!! Form::radio('type', 'Commercial', ['class' => 'form-control']) !!} Commercial--}}
            {{--{!! Form::radio('type', 'Both', ['class' => 'form-control']) !!} Both--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::label('status', 'Status:') !!}--}}
            {{--{!! Form::radio('status', 'Booked', ['class' => 'form-control']) !!} Booked--}}
            {{--{!! Form::radio('status', 'Available', ['class' => 'form-control']) !!} Available--}}

        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::label('readyToMove', 'Ready To Move:') !!}--}}
            {{--{!! Form::radio('readyToMove', 'Yes', ['class' => 'form-control']) !!} Yes--}}
            {{--{!! Form::radio('readyToMove', 'No', true) !!} No--}}

        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::label('address', 'Address:') !!}--}}
            {{--{!! Form::textarea('address', null, ['class'=>'form-control', 'rows'=>5] ) !!}--}}

        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::label('city', 'City:') !!}--}}
            {{--{!! Form::text('city', null, ['class' => 'form-control']) !!}--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::label('country', 'Country:') !!}--}}
            {{--{!! Form::text('country', null, ['class' => 'form-control']) !!}--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::label('mapLocation', 'Map Location:') !!}--}}
            {{--{!! Form::text('mapLocation', null, ['class' => 'form-control']) !!}--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::label('landArea', 'Land Area:') !!}--}}
            {{--{!! Form::text('landArea', null, ['class' => 'form-control']) !!} sq.ft--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::label('houseArea', 'House Area:') !!}--}}
            {{--{!! Form::text('houseArea', null, ['class' => 'form-control']) !!} sq.ft--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::label('plotted', 'Plotted:') !!}--}}
            {{--{!! Form::radio('plotted', 'Yes', ['class' => 'form-control']) !!} Yes--}}
            {{--{!! Form::radio('plotted', 'No', ['class' => 'form-control']) !!} No--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::label('storey', 'Storey:') !!}--}}
            {{--{!! Form::text('storey', null, ['class' => 'form-control']) !!}--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::label('bedroom', 'Bedroom:') !!}--}}
            {{--{!! Form::text('bedroom', null, ['class' => 'form-control']) !!}--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::label('bathroom', 'Bathroom:') !!}--}}
            {{--{!! Form::text('bathroom', null, ['class' => 'form-control']) !!}--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::label('kitchen', 'Kitchen:') !!}--}}
            {{--{!! Form::text('kitchen', null, ['class' => 'form-control']) !!}--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::label('roadDistance', 'Road Distance:') !!}--}}
            {{--{!! Form::text('roadDistance', null, ['class' => 'form-control']) !!} meter--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::label('description', 'Description:') !!}--}}
            {{--{!! Form::textarea('description', null, ['class'=>'form-control', 'rows'=>5] ) !!}--}}

        {{--</div>--}}



        {{--<div class="form-group">--}}
            {{--{!! Form::label('image', 'Choose an image') !!}--}}
            {{--{!! Form::file('image') !!}--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::submit('Save', array( 'class'=>'btn btn-danger form-control' )) !!}--}}
        {{--</div>--}}







    {{--{!! Form::close() !!}--}}
    {{--</div>--}}

@endsection